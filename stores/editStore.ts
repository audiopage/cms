import { defineStore } from 'pinia';
import type { AlbumInfo, TrackInfo } from '~/utils/types';

// Not currently used - but could stage pending changes before writing
// them into IndexedDB
//
// - in useAudiopageData() to combine pending changes with the indexeddb state

export type EditAction = {
	type: "WriteAlbum",
	siteId: number,
	album: AlbumInfo,
} | {
	type: "WriteTrack",
	siteId: number,
	track: TrackInfo,
};

export const useEditStore = defineStore("editStore", {
	state: () => ({
		actions: [] as EditAction[],
	}),
});
