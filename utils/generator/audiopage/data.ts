import type { SiteData } from "~/utils/hooks/useSiteData";
import type { AudiopageData, AudiopageTrack } from "./types";

export async function createAudiopageData(data: SiteData): Promise<AudiopageData> {
	const tracks: AudiopageTrack[] = await Promise.all(
		data.tracks.map((track) => ({
			slug: track.title,
			title: track.title,
			audioSources: [],
		}))
	);

	return {
		tracks,
	};
}
