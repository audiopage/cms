import { FFmpeg } from '@ffmpeg/ffmpeg';
import { fetchFile, toBlobURL } from '@ffmpeg/util';
import type { AudioSourceStream, Encoder } from './base';
import { v4 as uuidv4 } from 'uuid';

const ffmpeg: FFmpeg = import.meta.server ? undefined! : new FFmpeg();
ffmpeg?.on("log", ({ message }) => {
	console.log("ffmpeg:", message);
});
ffmpeg?.on("progress", ({ progress }) => {
});

function isAvailable() {
	return true;
}

function streamOf(arr: Uint8Array): ReadableStream<Uint8Array> {
	return new ReadableStream({
		async start(controller) {
			controller.enqueue(arr);
			controller.close();
		}
	});
}

async function ffmpegWrite(sourceFile: File, filePath: string) {
	const isWriteSuccessful = await ffmpeg.writeFile(filePath, await fetchFile(sourceFile));
	if (!isWriteSuccessful) {
		throw new Error(`Failed to write file: ${sourceFile.name} -> ${filePath}`);
	}
}

async function ffmpegDelete(filePath: string) {
	await ffmpeg.deleteFile(filePath).then(isSuccess => {
		if (!isSuccess) {
			throw new Error(`Failed to delete file: ${filePath}`);
		}
	});
}

async function ffmpegRead(filePath: string): Promise<Uint8Array> {
	const file = await ffmpeg.readFile(filePath);
	if (!file || typeof file === "string") {
		throw new Error(`Unsuitable file: ${filePath}`);
	} else {
		return file;
	}
}

async function* encode(sourceFile: File): AsyncGenerator<AudioSourceStream> {
	await ffmpeg.load({
		coreURL: await toBlobURL("/generated/ffmpeg/ffmpeg-core.js", 'text/javascript'),
		wasmURL: await toBlobURL("/generated/ffmpeg/ffmpeg-core.wasm", 'application/wasm'),
		workerURL: await toBlobURL("/generated/ffmpeg/ffmpeg-core.worker.js", 'text/javascript'),
	});

	const extensionRegex = /\.(\w+)$/.exec(sourceFile.name);
	if (!extensionRegex || extensionRegex.length < 2) throw new Error(`File name ${sourceFile.name} does not have a valid extension?`);
	const extension = extensionRegex[1];

	const sourceUuid = uuidv4();
	const sourceFilePath = `${sourceUuid}.${extension}`;

	// Write the file to ffmpeg fs
	await ffmpegWrite(sourceFile, sourceFilePath);

	// --- OGG/OPUS encoding:

	// The ffmpeg.wasm libopus encoding is buggy:
	// https://github.com/ffmpegwasm/ffmpeg.wasm/issues/591
	/*{
		const opusPath = `${sourceUuid}.opus`;
		await ffmpeg.exec([
			"-i", sourceFilePath,
			"-c:a", "libopus", // use libopus
			"-ar", "48k", // audio sampling rate: 48khz
			"-b:a", "96k", // audio bitrate: 96kbps
			"-map_metadata", "-1", // remove any metadata / album art
			opusPath,
		]).then(exitCode => {
			if (exitCode != 0) {
				throw new Error(`Encoding ${sourceFile.name} failed with exit code ${exitCode}!`);
			}
		});

		yield {
			stream: streamOf(await ffmpeg.readFile(opusPath) as Uint8Array),
			mimeType: "audio/ogg; codecs=opus",
		};

		await ffmpegDelete(opusPath);
	}*/

	// --- OGG/VORBIS encoding:

	{
		const oggPath = `${sourceUuid}.ogg`;
		await ffmpeg.exec([
			"-i", sourceFilePath,
			"-c:a", "libvorbis", // use libvorbis
			"-ar", "48k", // audio sampling rate: 48khz
			"-b:a", "128k", // audio bitrate: 128kbps
			"-map_metadata", "-1", // remove any metadata / album art
			oggPath,
		]).then(exitCode => {
			if (exitCode != 0) {
				throw new Error(`Encoding ${sourceFile.name} failed with exit code ${exitCode}!`);
			}
		});

		yield {
			stream: streamOf(await ffmpegRead(oggPath)),
			mimeType: "audio/ogg; codecs=vorbis",
		};

		await ffmpegDelete(oggPath);
	}

	// --- MP3 encoding:

	{
		const mp3Path = `${sourceUuid}.mp3`;
		await ffmpeg.exec([
			"-i", sourceFilePath,
			"-c:a", "libmp3lame", // use libmp3lame
			"-ar", "44100", // audio sampling rate: 44.1khz
			"-b:a", "192k", // audio bitrate: 192kbps
			"-map_metadata", "-1", // remove any metadata / album art
			mp3Path,
		]).then(exitCode => {
			if (exitCode != 0) {
				throw new Error(`Encoding ${sourceFile.name} failed with exit code ${exitCode}!`);
			}
		});

		yield {
			stream: streamOf(await ffmpeg.readFile(mp3Path) as Uint8Array),
			mimeType: "audio/mpeg",
		};

		await ffmpegDelete(mp3Path);
	}

	// Cleanup to remove the source file after all processing
	await ffmpegDelete(sourceFilePath);
}

export const ffmpegEncoder: Encoder = {
	isAvailable,
	encode,
};
