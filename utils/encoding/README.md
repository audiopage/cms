Encoding takes place in two different implementations:

- Using the [WebCodecs API](https://developer.mozilla.org/en-US/docs/Web/API/WebCodecs_API)
- Using [ffmpeg.wasm](https://ffmpegwasm.netlify.app/docs/overview), to support browsers that do not implement WebCodecs (Firefox)

While ffmpeg.wasm is quite fast, it also has a large bundle size, and cannot stream files during encoding.

WebCodecs might have the advantage that files can be streamed directly from their source on-disk to their destination, when saved by the user. As such, this would be a great advantage if it can be implemented in the future.

For now, ffmpeg.wasm seems to work fine...

These are used to generate streaming files in the following formats (see [ffmpeg's audio encoding guide](https://trac.ffmpeg.org/wiki/Encode/HighQualityAudio) for the reasoning here)
- .opus, libopus, 96kbps
- .ogg, libvorbis, 128kbps
- .mp3, libmp3lame, 192kbps
