import type { AudioMimeType } from "../types";

export type AudioSourceStream = {
	stream: ReadableStream<Uint8Array>
	mimeType: AudioMimeType;
};

export interface Encoder {
	isAvailable(): boolean;
	encode(file: File): AsyncGenerator<AudioSourceStream>;
}
