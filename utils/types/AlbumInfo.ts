export type AlbumInfo = {
	id?: number;
	siteId: number,

	title: string;
	createdAt: Date;
	updatedAt: Date;
};
