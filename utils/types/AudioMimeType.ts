export type AudioMimeType =
	| "audio/ogg; codecs=opus"
	| "audio/ogg; codecs=vorbis"
	| "audio/mpeg";
