import type { AudioMimeType } from "./AudioMimeType";

export type AudiopageTrackSource = {
	filePath: string,
	mimeType: AudioMimeType,
};

export type AudiopageTrack = {
	slug: string,
	title: string,
	audioSources: AudiopageTrackSource[],
};

export type AudiopageData = {
	tracks: AudiopageTrack[],
};
