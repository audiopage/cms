export type TrackInfo = {
	id?: number,
	siteId: number,
	albumId: number,

	title: string,
	createdAt: Date;
	updatedAt: Date;
	fileName: string,
	fileHandle: FileSystemFileHandle,

	fileHash?: string,
	cacheId?: string,
};
