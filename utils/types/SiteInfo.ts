export type SiteInfo = {
	id?: number,
	name: string,
	baseUrl?: string,

	createdAt: Date;
	updatedAt: Date;
};
