export type ArtistInfo = {
	id?: number,
	siteId: number,
	name: string,
};
