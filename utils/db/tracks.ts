import { db } from ".";
import type { TrackInfo } from "../types/TrackInfo";

export async function addTracks(tracks: TrackInfo[]) {
	await db?.tracks.bulkAdd(tracks);
}

export async function updateTrack(id: number, info: Partial<TrackInfo>) {
	await db?.tracks
		.where("id")
		.equals(id)
		.modify(info);
}

export async function deleteTrack(id: number) {
	await db?.tracks
		.where("id")
		.equals(id)
		.delete();
}
