import Dexie, { type Table } from 'dexie';
import type { TrackInfo, AlbumInfo, ArtistInfo, SiteInfo } from '../types';

export class AudiopageDexie extends Dexie {
	tracks!: Table<TrackInfo>;
	albums!: Table<AlbumInfo>;
	artists!: Table<ArtistInfo>;
	sites!: Table<SiteInfo>;

	constructor() {
		super('AudiopageDB');
		this.version(3).stores({
			tracks: '++id, albumId, siteId, createdAt',
			albums: '++id, siteId, createdAt',
			artists: '++id, siteId',
			sites: '++id',
		});
	}
}

export const db = import.meta.server ? undefined : new AudiopageDexie();
