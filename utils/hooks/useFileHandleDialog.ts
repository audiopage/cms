import { createEventHook, useFileDialog, useSupported } from "@vueuse/core";
import { v4 as uuidv4 } from "uuid";

export type FileHandleDialogOptions = {
	id: string,
	accept: string,
	extensions: string[],
	directory: boolean,
	multiple: boolean,
};

export function useFileHandleDialog(options: FileHandleDialogOptions) {
	const legacyDialog = useFileDialog(options);

	const isSupported = useSupported(() => window && 'showSaveFilePicker' in window && 'showOpenFilePicker' in window);

	const openEvent = createEventHook<FileSystemFileHandle[]>();

	legacyDialog.onChange(async () => {
		const files = legacyDialog.files.value;
		if (files == null) return;

		let fileHandles: FileSystemFileHandle[] = [];

		const root = await navigator.storage.getDirectory();
		for (const file of files) {
			const fileUuid = uuidv4();
			const fileHandle = await root.getFileHandle(`upload-${fileUuid}-${file.name}`);

			// Write the uploaded file
			const writableStream = await fileHandle.createWritable();
			await writableStream.write(file);
			await writableStream.close();

			// Push to the fileHandles array
			fileHandles.push(fileHandle);
		}

		openEvent.trigger(fileHandles);
	});

	return {
		async open() {
			if (isSupported) {
				// https://developer.mozilla.org/en-US/docs/Web/API/Window/showOpenFilePicker
				const fileList = await (window as any).showOpenFilePicker({
					id: options.id,
					multiple: options.multiple,
					startIn: "music",
					types: [{
						accept: {
							[options.accept]: options.extensions,
						},
					}],
				});
				openEvent.trigger([...fileList]);
			} else {
				// If showOpenFilePicker is not supported (firefox), open the legacy file dialog
				legacyDialog.open();
			}
		},
		onFilePicked: openEvent.on,
	}
}
