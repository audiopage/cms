import type { AlbumInfo, ArtistInfo, SiteInfo, TrackInfo } from "~/utils/types";
import { db } from "~/utils/db";
import { useEditStore } from "~/stores/editStore";
import { liveQuery, type Subscription } from "dexie";

export type SiteData = {
	site: SiteInfo,
	artists: ArtistInfo[],
	albums: AlbumInfo[],
	tracks: TrackInfo[],
};

export function useStoredSiteData(siteIdRef: MaybeRef<number|undefined>) {
	const siteData = ref<SiteData>();
	if (!db) return { siteData };

	watchEffect((onCleanup) => {
		const siteId = unref(siteIdRef);
		if (siteId === undefined || !isFinite(siteId))
			return;

		const observable = liveQuery(async () => {
			return await Promise.all([
				db!.tracks.where({ siteId }).toArray(),
				db!.albums.where({ siteId }).toArray(),
				db!.artists.where({ siteId }).toArray(),
				db!.sites.get(unref(siteId)),
			])
		});

		let subscription: Subscription|undefined = observable.subscribe({
			next([tracks, albums, artists, site]) {
				siteData.value = {
					tracks, albums, artists,
					site: site!,
				};
			}
		});

		onCleanup(() => {
			subscription?.unsubscribe();
			subscription = undefined;
		});
	});

	return { siteData };
}

export function usePreviewSiteData(siteId: MaybeRef<number|undefined>) {
	const { siteData } = useStoredSiteData(siteId);
	const editedData = ref<SiteData>();
	if (!db) return { siteData, editedData };

	const editStore = useEditStore();

	watch([siteData, editStore.actions], ([data, edits]) => {
		if (!data) return;

		for (const edit of edits) {
			if (edit.siteId !== data.site.id) continue;

			if (edit.type === "WriteAlbum") {
				data.albums = data.albums
					.filter(a => a.id !== edit.album.id)
					.concat([edit.album]);
			}

			if (edit.type === "WriteTrack") {
				data.tracks = data.tracks
					.filter(t => t.id !== edit.track.id)
					.concat([edit.track]);
			}
		}

		editedData.value = data;
	});

	return { siteData, editedData };
}
