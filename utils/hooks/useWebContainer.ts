import { createEventHook, createSharedComposable, useAsyncState } from '@vueuse/core';
import { WebContainer, type WebContainerProcess } from '@webcontainer/api';
import { unzipSync } from 'fflate';
import { liveQuery } from 'dexie';
import { useObservable, from } from '@vueuse/rxjs';
import { createAudiopageData } from '~/utils/generator/audiopage';
import type { AudiopageData } from '~/utils/generator/audiopage/types';

export const instance = ref<WebContainer>();
export const instanceUrl = ref<string>()
let instanceCreated = false;

let ready: Promise<void> = Promise.resolve();
let currentProcess: WebContainerProcess|undefined = undefined;
let currentProcessType: "dev"|undefined = undefined;

function requireInstance() {
	const i = instance.value;
	if (!i) throw new Error("WebContainer instance not started!");
	return i;
}

async function internalCreateInstance() {
	if (instance.value) return;
	// This should only run once!
	const i = instance.value = await WebContainer.boot();

	i.on("server-ready", (port, url) => {
		instanceUrl.value = url;
	});
}

async function internalRunInstall() {
	const i = requireInstance();

	console.log("Fetching audiopage.zip...");
	// Download & extract audiopage source from zip
	const contentBuffer = await fetch('/generated/audiopage.zip').then(r => r.arrayBuffer());
	const contents = unzipSync(new Uint8Array(contentBuffer));

	console.log("Write audiopage.zip...");
	await i.fs.writeFile("audiopage.zip", new Uint8Array(contentBuffer));
	for (const [path, fileArr] of Object.entries(contents)) {
		if (fileArr.length === 0) {
			await i.fs.mkdir(path);
		} else {
			await i.fs.writeFile(path, fileArr);
		}
	}

	console.log("pnpm install...");
	const installProcess = await i.spawn('pnpm', ['install', '--prefer-offline', '--frozen-lockfile']);
	if (await installProcess.exit !== 0) {
	 	throw new Error('Unable to run pnpm install');
	}
}

async function internalRunDev() {
	if (currentProcess && currentProcessType === "dev")
		return;

	currentProcess?.kill();
	instanceUrl.value = undefined;

	const i = requireInstance();

	console.log("pnpm run dev...");
	currentProcess = await i.spawn('pnpm', ['run', 'dev'], { env: { AUDIOPAGE_ENV: "webcontainer" } });
	currentProcessType = "dev";
}

export async function createInstance() {
	if (import.meta.server) return;
	if (instanceCreated) return;
	instanceCreated = true;

	await ready;
	ready = (async () => {
		await internalCreateInstance();
		await internalRunInstall();
	})();
	await ready;
}

export function useDevServer() {
	if (import.meta.server) return {};
	if (currentProcess) return {};

	(async () => {
		await createInstance();
		ready = internalRunDev();
	})();

	return { instance, instanceUrl };
}
