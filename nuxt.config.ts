import symlink from "symlink-dir";
import { zip } from "zip-a-folder";
import * as path from "path";
import * as fs from "fs/promises";

async function preBuild() {
	// Place the ffmpeg core files in the "/generated/ffmpeg" folder
	// (these need to be fetched by URL at runtime)
	await symlink(path.resolve("node_modules/@ffmpeg/core-mt/dist/esm"), path.resolve("public/generated/ffmpeg"));

	// Zip the audiopage UI into /generated/audiopage.zip
	await zip(await fs.realpath("node_modules/audiopage"), path.resolve("public/generated/audiopage.zip"));

	// https://www.npmjs.com/package/coi-serviceworker
	await fs.copyFile(path.resolve("node_modules/coi-serviceworker/coi-serviceworker.min.js"), path.resolve("public/coi-serviceworker@0.1.7.min.js"));
}

// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
	modules: [preBuild, "@pinia/nuxt"],
	devtools: { enabled: true },
	vite: {
		optimizeDeps: {
			exclude: ['@ffmpeg/ffmpeg'],
		},
	},
})