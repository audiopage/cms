import { setHeader } from 'h3';

/**
 * These headers are required to use the ffmpeg WASM build:
 * https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/SharedArrayBuffer#security_requirements
 */
export default defineNuxtRouteMiddleware((to) => {
	if (import.meta.server) {
		const req = useRequestEvent();
		setHeader(req, 'Cross-Origin-Embedder-Policy', 'require-corp');
		setHeader(req, 'Cross-Origin-Opener-Policy', 'same-origin');
	}
});
